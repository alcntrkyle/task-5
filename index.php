<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task 5</title>
</head>
<body>
     <form action="" method="post">
        <label for="name">Enter your name: </label>
        <input type="text" id="name" name="name"> <br>
        <input type="submit" value="Submit">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $userName = $_POST['name'];
        echo "Hello, $userName! Welcome to our website.";
    }
    ?>
</body>
</html>